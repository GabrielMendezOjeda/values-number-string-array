#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int valuesNumber(const char* foundValue, const char** value, int valueLength) {
	int nameamount = 0;

	    for(int k = 0; k < valueLength; k++) {
	        if (strcmp(value[k], foundValue) == 0) {
	             nameamount++;
	        }
	    }

	    return nameamount;
	}
