#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jint JNICALL Java_library_valuesNumber_1
  (JNIEnv *env, jobject object, jstring foundValue, jobjectArray value, jint valueLength)
{
    javaEnv = env;
    const char* c_foundValue = toString(foundValue);
    int c_valueLength = toInt(valueLength);
    const char** c_value = toStringArray(value, c_valueLength);
    int c_outValue = valuesNumber(c_foundValue, c_value, c_valueLength);
    setJstringArray(value , c_value, &c_valueLength);
    return toJint(c_outValue);
}
