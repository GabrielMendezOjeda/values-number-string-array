/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "valuesNumber(const char*,const char**,int):int")
    public int valuesNumber(String foundValue, String[] value, int valueLength){
        return valuesNumber_(foundValue, value, valueLength);
    }
    private native int valuesNumber_(String foundValue, String[] value, int valueLength);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
