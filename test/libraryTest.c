#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
	// Given
	    const char* name[] = {"Javier"};
	    // When
	    int amount = valuesNumber("Eva", name, 1) ;
	    // Then
	    assertEquals_int(0, amount);
	}


void testExercise_B() {
	// Given
		    const char* name[] = {"Alba"};
		    // When
		    int amount = valuesNumber("Alba", name, 1) ;
		    // Then
		    assertEquals_int(1, amount);
		}

void testExercise_C() {
	// Given
		    const char* name[] = {"Manuel", "Manuel"};
		    // When
		    int amount = valuesNumber("Manuel", name, 2) ;
		    // Then
		    assertEquals_int(2, amount);
		}


void testExercise_D() {
	// Given
		const char* name[] = {"Susana", "Susana", "Eva", "Susana", "Susana", "Manuel"};
	// When
		 int amount = valuesNumber("Susana", name, 6) ;
	 // Then
		 assertEquals_int(4, amount);
			}


void testExercise_E() {
	// Given
			const char* name[] = {"Susana", "Blanca", "Javier", "Blanca", "Manuel", "Eva", "Manuel", "Alba", "Blanca"};
	// When
			 int amount = valuesNumber("Blanca", name, 9) ;
	// Then
			 assertEquals_int(3, amount);
				}
